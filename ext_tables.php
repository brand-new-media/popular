<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Popular',
            'Counter',
            'Popular Counter'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('popular', 'Configuration/TypoScript', 'Beliebte Inhalte');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_popular_domain_model_counter', 'EXT:popular/Resources/Private/Language/locallang_csh_tx_popular_domain_model_counter.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_popular_domain_model_counter');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$pluginSignature = 'popular_counter';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:popular/Configuration/FlexForms/flexform_counter.xml');