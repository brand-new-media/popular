<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Popular',
            'Counter',
            [
                \BNM\Popular\Controller\CounterController::class => 'code, write'
            ],
            // non-cacheable actions
            [
                \BNM\Popular\Controller\CounterController::class => 'write'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        counter {
                            iconIdentifier = popular-plugin-counter
                            title = LLL:EXT:popular/Resources/Private/Language/locallang_db.xlf:tx_popular_counter.name
                            description = LLL:EXT:popular/Resources/Private/Language/locallang_db.xlf:tx_popular_counter.description
                            tt_content_defValues {
                                CType = list
                                list_type = popular_counter
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'popular-plugin-counter',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:popular/Resources/Public/Icons/user_plugin_counter.svg']
			);

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/**
 * ViewHelper Namespace deklarieren
 */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['fluid']['namespaces']['pop'] = ['BNM\\Popular\\ViewHelpers'];

/**
 * Scheduler Jobs
 */

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'BNM\\Popular\\Command\\AbstractCommandController';
