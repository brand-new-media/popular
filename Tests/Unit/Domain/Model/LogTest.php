<?php
declare(strict_types=1);
namespace BNM\Popular\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class LogTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \BNM\Popular\Domain\Model\Log
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \BNM\Popular\Domain\Model\Log();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getModelReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getModel()
        );
    }

    /**
     * @test
     */
    public function setModelForStringSetsModel()
    {
        $this->subject->setModel('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'model',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getForeignUidReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getForeignUid()
        );
    }

    /**
     * @test
     */
    public function setForeignUidForIntSetsForeignUid()
    {
        $this->subject->setForeignUid(12);

        self::assertAttributeEquals(
            12,
            'foreignUid',
            $this->subject
        );
    }
}
