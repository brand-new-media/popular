<?php
declare(strict_types=1);
namespace BNM\Popular\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Axel Brand <kontakt@brandnewmedia.it>
 */
class CounterTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \BNM\Popular\Domain\Model\Counter
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \BNM\Popular\Domain\Model\Counter();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTablenameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTablename()
        );
    }

    /**
     * @test
     */
    public function setTablenameForStringSetsTablename()
    {
        $this->subject->setTablename('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'tablename',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getUidForeignReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getUidForeign()
        );
    }

    /**
     * @test
     */
    public function setUidForeignForIntSetsUidForeign()
    {
        $this->subject->setUidForeign(12);

        self::assertAttributeEquals(
            12,
            'uidForeign',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStatusReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getStatus()
        );
    }

    /**
     * @test
     */
    public function setStatusForIntSetsStatus()
    {
        $this->subject->setStatus(12);

        self::assertAttributeEquals(
            12,
            'status',
            $this->subject
        );
    }
}
