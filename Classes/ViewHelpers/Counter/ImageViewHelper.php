<?php

namespace BNM\Popular\ViewHelpers\Counter;

use BNM\Popular\Domain\Model\Counter;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception;

class ImageViewHelper extends AbstractTagBasedViewHelper
{

    /**
     * @var string
     */
    protected $tagName = 'img';

    /**
     * Initialize arguments.
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerArgument('tablename', 'string', 'Specifies an alternate text for an image', false);
        $this->registerArgument('uid', 'integer', 'Uid of record to count', false);
        $this->registerArgument('type', 'integer', 'typeNum of Page', false, 1606044192);
    }

    /**
     * Resizes a given image (if required) and renders the respective img tag
     *
     * @see https://docs.typo3.org/typo3cms/TyposcriptReference/ContentObjects/Image/
     *
     * @throws Exception
     * @return string Rendered tag
     */
    public function render()
    {
        $tablename = (string)$this->arguments['tablename'];
        if (!$tablename) {
            throw new Exception('You must either specify a string tablename.', 1382284106);
        }

        $uidForeign = (int)$this->arguments['uid'];
        if (!$uidForeign) {
            throw new Exception('You must either specify a integer uid.', 1382294106);
        }

        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $objectManager->get(UriBuilder::class);

        $uri = $uriBuilder->reset()
            ->setTargetPageType($this->arguments['type'])
            ->uriFor(
            'write',
            ['tablename' => $tablename, 'uid' => $uidForeign],
            'Counter',
            'popular',
            'Counter'
        );

        $this->tag->addAttribute('src', $uri);
        $this->tag->addAttribute('width', "1");
        $this->tag->addAttribute('height', "1");

        return $this->tag->render();
    }
}
