<?php
declare(strict_types=1);

namespace BNM\Popular\Domain\Model;

use BNM\Portal\Traits\Model\Crdate;

/***
 *
 * This file is part of the "Beliebte Inhalte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * Log
 */
class Counter extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    use Crdate;

    /**
     * tablename
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $tablename = '';

    /**
     * uidForeign
     * 
     * @var int
     */
    protected $uidForeign = 0;

    /**
     * status
     * 
     * @var int
     */
    protected $status = 0;

    /**
     * Returns the tablename
     * 
     * @return string tablename
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * Sets the tablename
     * 
     * @param string $tablename
     * @return void
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;
    }

    /**
     * Returns the uidForeign
     * 
     * @return int uidForeign
     */
    public function getUidForeign()
    {
        return $this->uidForeign;
    }

    /**
     * Sets the uidForeign
     * 
     * @param int $uidForeign
     * @return void
     */
    public function setUidForeign($uidForeign)
    {
        $this->uidForeign = $uidForeign;
    }

    /**
     * Returns the status
     * 
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     * 
     * @param int $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
