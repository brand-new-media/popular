<?php
declare(strict_types=1);

namespace BNM\Popular\Domain\Repository;

use TYPO3\CMS\Extbase\Annotation;

/***
 *
 * This file is part of the "Beliebte Inhalte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * The repository for Logs
 */
class CounterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @Annotation\Inject
     */
    protected $persistenceManager = null;


    public function getTablenamesToCount() {
        $query = $this->createQuery();
        $statement = "SELECT tablename FROM tx_popular_domain_model_counter WHERE status=0 GROUP BY tablename";
        $query->statement($statement);
        return $query->execute(true);
    }
}
