<?php
declare(strict_types=1);

namespace BNM\Popular\Controller;

use BNM\Popular\Domain\Model\Counter;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation;

/***
 *
 * This file is part of the "Beliebte Inhalte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Axel Brand <kontakt@brandnewmedia.it>, brand new media
 *
 ***/
/**
 * CounterController
 */
class CounterController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * counterRepository
     * 
     * @var \BNM\Popular\Domain\Repository\CounterRepository
     * @Annotation\Inject
     */
    protected $counterRepository = null;

    /**
     * Persistence Manager
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @Annotation\Inject
     */
    protected $persistenceManager = null;

    /**
     * action write
     * 
     * @param string $tablename
     * @param integer $uid
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @return void
     */
    public function writeAction($tablename = '', $uid = null)
    {
        if ($tablename && $uid) {

            /** @var Counter $counter */
            $counter = GeneralUtility::makeInstance(Counter::class);
            $counter->setTablename($tablename);
            $counter->setUidForeign($uid);
            $this->counterRepository->add($counter);
            $this->persistenceManager->persistAll();
        }
        header('Content-Type: image/gif');
        readfile($this->settings['counterImage']);
        die;
    }

    /**
     * action code
     * 
     * @return void
     */
    public function codeAction()
    {
    }

    /**
     * @param \BNM\Popular\Domain\Repository\CounterRepository $counterRepository
     */
    public function injectCounterRepository(\BNM\Popular\Domain\Repository\CounterRepository $counterRepository)
    {
        $this->counterRepository = $counterRepository;
    }
}
