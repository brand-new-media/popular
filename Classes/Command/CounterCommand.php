<?php


namespace BNM\Popular\Command;


use BNM\Popular\Domain\Repository\CounterRepository;
use BNM\Portal\Controller\AdminController;
use In2code\Powermail\Utility\ObjectUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class CounterCommand extends Command
{

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     */
    protected $persistenceManager = null;

    /**
     * @var \BNM\Popular\Domain\Repository\CounterRepository
     * @Annotation\Inject
     */
    protected $counterRepository = null;

    /**
     * @var ObjectManager
     */
    protected $objectManager = null;
    /**
     * Typoscript
     *
     * @var array
     */
    protected $settings = [];

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        /**
         * Init ObjectManager
         */
        $this->objectManager = ObjectUtility::getObjectManager();
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);

        /**
         * Init ConfigurationManager for getting typoscript settings
         *
         * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
         */
        $configurationManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);

        /**
         * typoscript settings of extension
         */
        $this->settings = $configurationManager->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'popular'
        );

        /**
         * Inject Repositories
         */
        $this->counterRepository = $this->objectManager->get(CounterRepository::class);
    }

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Calculate the hits')
            ->setHelp('Counter' . LF . 'If you want to get more detailed information, use the --verbose option.');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $tablenames = $this->counterRepository->getTablenamesToCount();

        $counterArr = [];
        /** @var string $tablename */
        foreach ($tablenames as $tablename) {
            $io->writeln('Table: ' . $tablename['tablename']);
        }

        return Command::SUCCESS;
    }


    public function counterCommand() {
    }
}
